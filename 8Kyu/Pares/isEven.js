﻿/* 
Queremos que nuestra función 'isEven' retorne un array con todos los números pares que encuentre en el array que nos llega por parámetro.

Link: https://courseit.io/desafios/008
*/

function isEven(arr) {
  let aux = [];
  arr.map(numero => numero % 2 == 0 ? aux.push(numero) : [])
  return aux;
}

console.log(isEven([2]))
console.log(isEven([4, 2, 1, -10000, -7]))
console.log(isEven([3, -23, 1, 3, 1337]))
console.log(isEven([4, 0, 0, 4, 0, 0, 4]))