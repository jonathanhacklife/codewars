﻿/* 
Simple, remove the spaces from the string, then return the resultant string.

Link: https://www.codewars.com/kata/57eae20f5500ad98e50002c5
*/

const noSpace = (x) => x.split(" ").join("")

console.log(noSpace('8 j 8   mBliB8g  imjB8B8  jl  B'))