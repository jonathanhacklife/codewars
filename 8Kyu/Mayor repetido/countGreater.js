﻿/* 
Queremos saber cuantas veces se repite el número más grande de un array. El valor que tiene que devolver la función es un número.

Link: https://courseit.io/desafios/006
*/

function countGreater(arr) {
  let numeroMayor = 0;
  let contador = 0;

  for (let i = 0; i < arr.length; i++) {
    if (arr[i] > numeroMayor) {
      numeroMayor = arr[i];
      contador = 1;
    }
    else if (arr[i] == numeroMayor) {
      contador++
    }
  }
  return contador
}

console.log(countGreater([6, 89, 0, 1]))
console.log(countGreater([200, 200, 200]))
console.log(countGreater([]))
console.log(countGreater([-100, 3, 3, -90, -2, 3]))