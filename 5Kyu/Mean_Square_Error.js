﻿/*
[X] Acepta dos matrices de enteros de igual longitud
[X] Compara el valor de cada miembro de una matriz con el miembro correspondiente de la otra
[X] Eleva al cuadrado la diferencia de valor absoluto entre esos dos valores
[X] Devuelve la media de esa diferencia de valor absoluto elevada al cuadrado entre cada par de miembros.

Ejemplos:
[1, 2, 3], [4, 5, 6]              -->   9   porque (9 + 9 + 9) / 3
[10, 20, 10, 2], [10, 25, 5, -2]  -->  16.5 porque (0 + 25 + 25 + 16) / 4
[-1, 0], [0, -1]                  -->   1   porque (1 + 1) / 2
*/

/* const solution = (firstArray, secondArray) => firstArray.map((actual, index) => (Math.abs(actual - secondArray[index]) ** 2)).reduce((prev, curr) => prev + curr) / firstArray.length; */

/* Solución alternativa (Más legible) */
const solution = (firstArray, secondArray) => {
  let total = 0;
  for (let i = 0; i < firstArray.length; i++)
    total += (firstArray[i] - secondArray[i]) ** 2;
  return total / firstArray.length;
}


console.log(solution([1, 2, 3], [4, 5, 6]))
console.log(solution([10, 20, 10, 2], [10, 25, 5, -2]))
console.log(solution([-1, 0], [0, -1]))