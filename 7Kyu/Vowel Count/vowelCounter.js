﻿/* 
Queremos hacer que la función 'vowelCounter' retorne la cantidad de vocales que tiene el parámetro 'str'

Link: https://courseit.io/desafios/017
*/

function vowelCounter(str) {
  let matchingInstances = str.match(/[aeiou]/gi);
  // Verifica si existe la instancia y luego calcula su longitud
  return matchingInstances ? matchingInstances.length : 0
}

console.log(vowelCounter("abracadabra patas de cabra"))