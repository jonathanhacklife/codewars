﻿""" Return the number (count) of vowels in the given string.

We will consider a, e, i, o, u as vowels for this Kata (but not y).

The input string will only consist of lower case letters and/or spaces.

Link: https://www.codewars.com/kata/54ff3102c1bad923760001f3 """

def get_count(input_str):
    vowels = 'aeiou'
    return len([x for x in range(len(input_str)) if input_str[x] in vowels])


    """ SOLUCION 1
    vowels = 'aeiou'
    for letter in input_str:
        if letter in vowels:
            num_vowels += 1
    return num_vowels
    """


print(get_count("abracadabra patas de cabra"))
