﻿/* Presentación:
Nos alegra que estés interesado en formar parte de ioet. Como parte del proceso de reclutamiento nos gustaría que resolvieras un ejercicio de programación para evaluar tus habilidades y posteriormente programaremos una reunión para discutir tu solución. Es importante destacar que puedes utilizar cualquier lenguaje de programación que quieras pero no puedes utilizar ninguna librería externa para resolver el ejercicio, sin embargo puedes añadir cualquier dependencia para realizar pruebas como JUnit, Mockito, etc.
*/

/* Instrucciones:
Este ejercicio debe completarse en un plazo de 7 días. Si por alguna razón no puede terminar a tiempo, por favor háganoslo saber.
*/

/* Ejercicio:
La empresa ACME ofrece a sus empleados la flexibilidad de trabajar las horas que deseen. Pagarán las horas trabajadas en función del día de la semana y la hora del día, según la siguiente tabla:

*Monday - Friday
00:01 - 09:00 25 USD
09:01 - 18:00 15 USD
18:01 - 00:00 20 USD

*Saturday and Sunday 
00:01 - 09:00 30 USD
09:01 - 18:00 20 USD
18:01 - 00:00 25 USD
*/

/* Objetivo:
El objetivo de este ejercicio es calcular el total que la empresa tiene que pagar a un empleado, en función de las horas que ha trabajado y el horario en el que lo ha hecho. Se utilizarán las siguientes abreviaturas para introducir los datos:

MO: Monday
TU: Tuesday
WE: Wednesday
TH: Thursday
FR: Friday
SA: Saturday
SU: Sunday

Entrada: el nombre de un empleado y el horario que ha trabajado, indicando el tiempo y las horas. Debe ser un archivo .txt con al menos cinco conjuntos de datos. Puede incluir los datos de nuestros dos ejemplos siguientes.

Salida: indicar cuánto hay que pagar al empleado
*/

/*Ejemplo:
Caso 1:
ENTRADA:
RENE=MO10:00-12:00,TU10:00-12:00,TH01:00-03:00,SA14:00-18:00,SU20:00-21:00

SALIDA:
The amount to pay RENE is: 215 USD

Case 2:
ENTRADA:
ASTRID=MO10:00-12:00,TH12:00-14:00,SU20:00-21:00

SALIDA:
The amount to pay ASTRID es: 85 USD
*/


/* Entrega:
Una vez que hayas terminado el ejercicio, sube la solución a GitHub y envíanos el enlace.No olvides incluir un archivo README.md.Tu README debe incluir una visión general de tu solución, una explicación de tu arquitectura, una explicación de tu enfoque y metodología e instrucciones de cómo ejecutar el programa localmente.

Evaluamos muchos aspectos, incluyendo lo bien estructurado que está su código, los diseños de patrones aplicados, las pruebas y la calidad de su solución.

La solución no debería necesitar ninguna interfaz de usuario, una aplicación de consola es suficiente.

Cuando envíe su ejercicio, asegúrese de evitar incluir archivos compilados, ya que esto podría ser considerado como malware. Por favor, incluya las instrucciones adecuadas para compilar su proyecto en el archivo README.
*/