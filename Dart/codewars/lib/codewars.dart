/* print(likes([]));
print(likes(['Peter']));
print(likes(['Jacob', 'Alex']));
print(likes(['Max', 'John', 'Mark']));
print(likes(['Alex', 'Jacob', 'Mark', 'Max'])); */


String likes(List names) => names.isEmpty
    ? 'no one likes this'
    : names.length == 1
        ? '${names[0]} likes this'
        : names.length == 2 ? '${names[0]} and ${names[1]} like this' : names.length == 3 ? '${names[0]}, ${names[1]} and ${names[2]} like this' : '${names[0]}, ${names[1]} and ${names.length-2} others like this';