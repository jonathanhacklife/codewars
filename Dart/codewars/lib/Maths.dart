import 'dart:io';
import 'dart:math';

Random random = Random();

void verdaderoOFalso() {
  // Seteamos las respuestas (Pueden ser textos, números o booleanos)
  String respuestaVerdadero = 'y';
  String respuestaFalso = 'n';
  int puntaje = 0;

  // Bucle
  while (true) {
    // Si el número al azar es 0 se reemplaza por 1 para no tener errores en el cálculo (además de que no sea tan sencilla la respuesta)
    int primerNumero = (random.nextInt(10) > 1) ? random.nextInt(10) : 1;
    int segundoNumero = (random.nextInt(10) > 1) ? random.nextInt(10) : 1;

    // Resultados
    int resultadoOriginal = primerNumero + segundoNumero;
    int resultadoFalso = max(primerNumero, segundoNumero) + random.nextInt(resultadoOriginal + min(primerNumero, segundoNumero));
    int resultadoAzar = random.nextBool() ? resultadoOriginal : resultadoFalso;

    // Afirmaciones
    bool esCorrecto = resultadoOriginal == resultadoAzar;
    bool esFalso = resultadoOriginal != resultadoFalso;

    /* La diferencia de "un número al azar" y un "resultado al azar" consiste en que en el
    primer caso se obtendría más respuestas incorrectas por su rango de error asignado
    (Se asigna un rango de error en base al número máximo de la operación y un número al azar
    entre el resultado original más el número mínimo de la operación)
    
    [EJEMPLO]
    minimo = 30
    maximo = 50
    resultado = 80

    rango min(numero máximo) = 50
    rango max = (50+30) + 30 => 110
     */
    print('$primerNumero + $segundoNumero = $resultadoAzar}\n¿Es verdadero o falso?\n');
    String seleccionUsuario = stdin.readLineSync();

    if ((seleccionUsuario == respuestaVerdadero) && esCorrecto || (seleccionUsuario == respuestaFalso) && esFalso) {
      print('¡CORRECTO!');
      puntaje++;
    } else {
      print('¡INCORRECTO!');
      break;
    }
  }
  // Mostrar el puntaje al final del bucle
  print('Tu puntaje es: $puntaje');
}