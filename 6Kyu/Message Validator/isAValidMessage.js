/* In this kata, you have an input string and you should check whether it is a valid message. To decide that, you need to split the string by the numbers, and then compare the numbers with the number of characters in the following substring.

For example "3hey5hello2hi" should be split into 3, hey, 5, hello, 2, hi and the function should return true, because "hey" is 3 characters, "hello" is 5, and "hi" is 2; as the numbers and the character counts match, the result is true.

Notes:

    Messages are composed of only letters and digits
    Numbers may have multiple digits: e.g. "4code13hellocodewars" is a valid message
    Every number must match the number of character in the following substring, otherwise the message is invalid: e.g. "hello5" and "2hi2" are invalid
    If the message is an empty string, you should return true

Link: https://www.codewars.com/kata/5fc7d2d2682ff3000e1a3fbc/javascript
*/

function isAValidMessage(mensaje) {
  let lista = mensaje.split(/(?<=\d)(?=\D)|(?=\d)(?<=\D)/);
  let lista_dividida = [];
  let resultado = true;

  for (let i = 0; i < lista.length; i += 2) {
    lista_dividida.push(lista.slice(i, i + 2));
  }
  lista_dividida.forEach((elemento) => {
    if (!(elemento.length == 2 && parseInt(elemento[0]) == elemento[1].length)) {
      resultado = false;
    }
  })
  return resultado || lista.length == 1;
}

// TESTs
console.log(isAValidMessage("3hey5hello2hi"))