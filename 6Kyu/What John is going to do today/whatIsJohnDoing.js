/*
John was a fisherman, living a very regular life. He always spent three days to go fishing, then rest for two days, for airing his fishing net. When he passed the three times fishing, he will use one day to sell his fish, and then airing his fishing net.

3 days fishing 
2 days airing net
3 days fishing
2 days airing net
3 days fishing
1 day selling fish
2 days airing net
3 days fishing 

Link: https://www.codewars.com/kata/5826ba6822be6e6c82000a36
*/

function whatIsJohnDoing(startDay, today) {
  let actions = "fffaafffaafffsaa"
  let response = { 'f': "Fishing", 'a': "Airing net", 's': "Selling fish" }
  return response[actions[(Date.parse(today) - Date.parse(startDay)) / 86400000 % actions.length]]
}
whatIsJohnDoing("2016-01-01","2017-01-01")