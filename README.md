# CodeWars nunca cambia.
Pero mi nivel de programación lo hace. Y así es como.

## Descripción

Estas son soluciones actualizadas que presenté en Codewars. Y es prácticamente un 'trabajo en progreso', ya que sigo una cita muy famosa:

>>>
Al mejorar continuamente el diseño del código, hacemos que sea más fácil trabajar con él.
Esto contrasta claramente con lo que suele suceder: poca refactorización y mucha atención para agregar nuevas funciones de manera oportuna.
Si adquiere el hábito higiénico de refactorizar continuamente, encontrará que es más fácil extender y mantener el código.

Joshua Kerievsky, Refactoring to Patterns
>>>

Lo digo en serio.\
Seriamente.

# Honores
![](https://www.codewars.com/users/JHacklife/badges/large)

[Ejercicios de CourseIT - Algoritmos](https://courseit.io/articulo/desafio-algoritmos-por-nivel-5tplp3at0)