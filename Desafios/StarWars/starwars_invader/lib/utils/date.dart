﻿import 'package:intl/intl.dart';

getCurrentDate() {
  /* "2011-11-03 11:13:39.278026" */
  return DateFormat('yyyy-MM-dd kk:mm:ss.ssssss').format(DateTime.now());
}
