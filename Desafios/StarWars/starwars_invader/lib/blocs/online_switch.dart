﻿import 'dart:async';
import 'package:connectivity_plus/connectivity_plus.dart';

class SwitchBLoC {
  StreamController switchController = StreamController.broadcast();
  Stream get getStreamConnectionMode => switchController.stream;

  Future<bool> checkDeviceConnection() async {
    var connectivityResult = await Connectivity().checkConnectivity();
    return connectivityResult != ConnectivityResult.none;
  }

  void setOnlineMode(bool mode) async {
    switchController.sink.add(mode);
    // Estado de conexión
    print("Cambiando a modo ${mode ? 'online' : 'offline'}");
  }

  dispose() {
    print("PASO POR EL DISPOSE");
    switchController.close();
  }
}
