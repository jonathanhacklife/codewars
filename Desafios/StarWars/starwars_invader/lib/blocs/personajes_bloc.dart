﻿import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:starwars_invader/apis/endpoints.dart';
import 'package:starwars_invader/apis/handle_error.dart';
import 'package:starwars_invader/utils/date.dart';
import 'package:starwars_invader/utils/shared.dart';

class PersonajesBLoC {
  StreamController personajesController = StreamController.broadcast();
  Stream get getStreamPersonajes => personajesController.stream;

  StreamController reporteController = StreamController.broadcast();
  Stream get getStreamReporte => reporteController.stream;

  Future<Map<String, dynamic>?> getPersonajes() async {
    try {
      var response = await http.get(Uri.parse(peopleURL), headers: {'Content-Type': 'application/json'});
      if (response.statusCode == 200) {
        Map<String, dynamic> decode = json.decode(response.body);
        log(decode.toString());
        deleteShared("swapiDB");
        saveData("swapiDB", decode);
        print("API");
        return decode;
      } else {
        handleError(response);
      }
    } on SocketException {
      throw 'No hay conexión a internet';
    }
    return null;
  }

  Future<Map<String, dynamic>?> reportarPersonaje(id, nombre) async {
    try {
      var response = await http.post(
        Uri.parse(reportURL),
        headers: {'Content-Type': 'application/json'},
        body: jsonEncode({"userId": id, "dateTime": getCurrentDate(), "character_name": nombre}),
      );
      if (response.statusCode == 201) {
        Map<String, dynamic> decode = json.decode(response.body);
        log(decode.toString());
        reporteController.sink.add(decode.toString());
        return decode;
      } else {
        handleError(response);
      }
    } on SocketException {
      throw 'No hay conexión a internet';
    }
    return null;
  }

  dispose() {
    print("PASO POR EL DISPOSE");
    personajesController.close();
    reporteController.close();
  }
}
