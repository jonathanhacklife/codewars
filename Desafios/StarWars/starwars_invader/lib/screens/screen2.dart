﻿import 'package:flutter/material.dart';
import 'package:starwars_invader/blocs/online_switch.dart';
import 'package:starwars_invader/blocs/personajes_bloc.dart';
import 'package:starwars_invader/styles/styles.dart';

class Screen2 extends StatefulWidget {
  const Screen2({Key? key}) : super(key: key);

  @override
  State<Screen2> createState() => _Screen2State();
}

class _Screen2State extends State<Screen2> {
  @override
  Widget build(BuildContext context) {
    final arguments = ModalRoute.of(context)?.settings.arguments as dynamic;
    final indx = arguments["indx"];
    final person = arguments["person"];
    final personajesBLoC = PersonajesBLoC();
    final switchBLoC = SwitchBLoC();

    return StreamBuilder<dynamic>(
        stream: personajesBLoC.getStreamReporte,
        builder: (context, snapshot) {
          return Scaffold(
            backgroundColor: dark,
            appBar: AppBar(
              title: Text("StarWars Invaders"),
              leading: BackButton(
                onPressed: () => Navigator.of(context).pop(),
              ),
              actions: [
                IconButton(
                  icon: const Icon(Icons.notifications_rounded),
                  onPressed: () {},
                ),
              ],
            ),
            bottomNavigationBar: FutureBuilder<dynamic>(
              future: switchBLoC.checkDeviceConnection(),
              builder: (context, deviceConnection) {
                if (deviceConnection.hasData && deviceConnection.data) {
                  return Padding(
                    padding: const EdgeInsets.all(20),
                    child: TextButton(
                      style: TextButton.styleFrom(padding: const EdgeInsets.all(20), backgroundColor: snapshot.hasData ? danger : warning),
                      onPressed: () => personajesBLoC.reportarPersonaje(indx, person.name),
                      child: Text(snapshot.hasData ? "REPORTADO" : "¡REPORTAR!", style: headline6(color: snapshot.hasData ? tertiary : dark)),
                    ),
                  );
                } else {
                  return Container();
                }
              },
            ),
            body: Center(
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    IDBanner(
                      id: person.birthYear,
                      title: person.name,
                      subtitle: person.homeworld,
                    ),
                    // FIXME: Cambiar formato según la orientación
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        // PANEL DE SUJETO
                        InfoPanel(
                          title: "Radar",
                          icon: Icons.radar_rounded,
                          color: snapshot.hasData ? warning : accent,
                          content: AvatarSW(image: radarImage, isSuspect: snapshot.hasData),
                        ),

                        // PANEL DE RECONOCIMIENTO FACIAL
                        InfoPanel(
                          title: "Recon. Facial",
                          icon: Icons.view_in_ar_rounded,
                          color: snapshot.hasData ? warning : accent,
                          content: DescriptionCard(info: person),
                        ),
                      ],
                    ),

                    // PANEL DE VEHÍCULOS
                    VehiclePanel(vehiculos: person.vehicles),
                  ],
                ),
              ),
            ),
          );
        });
  }
}

class IDBanner extends StatelessWidget {
  const IDBanner({
    Key? key,
    required this.id,
    required this.title,
    required this.subtitle,
  }) : super(key: key);
  final String id;
  final String title;
  final String subtitle;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Card(
              color: primary,
              child: Padding(
                padding: EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("ID: $id", style: bodyExtended(color: tertiary)),
                    Text(title, style: Theme.of(context).textTheme.headline4),
                    Text(subtitle, style: headline5(color: shadow)),
                  ],
                ),
              )),
        ),
      ],
    );
  }
}

class InfoPanel extends StatelessWidget {
  const InfoPanel({Key? key, required this.title, required this.icon, required this.color, required this.content}) : super(key: key);

  final String title;
  final IconData icon;
  final Color color;
  final Widget content;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            // HEADER
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10),
              child: Row(
                children: [
                  Expanded(
                    child: Container(
                      color: color,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 2),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Flexible(
                              child: Text(
                                title,
                                style: bodyText1(color: backgroundColor),
                                softWrap: false,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                            Icon(icon, color: backgroundColor),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            content,
          ],
        ),
      ),
    );
  }
}

class AvatarSW extends StatelessWidget {
  const AvatarSW({Key? key, required this.image, required this.isSuspect}) : super(key: key);

  final String image;
  final bool isSuspect;

  @override
  Widget build(BuildContext context) {
    return Stack(
      //fit: StackFit.loose,
      children: [
        Image.network(
          image,
          fit: BoxFit.cover,
          loadingBuilder: (context, child, loadingProgress) {
            if (loadingProgress == null) return child;
            return Center(child: CircularProgressIndicator());
          },
          errorBuilder: (context, error, stackTrace) => const Text('Some errors occurred!'),
        ),
        isSuspect
            ? Positioned(
                right: 0,
                left: 0,
                bottom: 0,
                child: Row(
                  children: [
                    Expanded(
                      child: Container(
                        padding: const EdgeInsets.all(10),
                        color: warning,
                        child: Expanded(
                          child: Text(
                            "SOSPECHOSO",
                            style: bodyText1(color: backgroundColor),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              )
            : Container(),
      ],
    );
  }
}

class DescriptionCard extends StatelessWidget {
  const DescriptionCard({Key? key, this.info}) : super(key: key);
  final dynamic info;

  @override
  Widget build(BuildContext context) {
    Map infoMap = {
      "Altura": "${info.height}m",
      "Peso": "${info.mass}kg",
      "Cabello": info.hairColor.toUpperCase(),
      "Ojos": info.eyeColor.toUpperCase(),
      "Piel": info.skinColor.toUpperCase(),
      "Género": info.gender,
    };
    return Container(
      padding: const EdgeInsets.all(10),
      color: primary,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          GridView.count(
            crossAxisCount: 2,
            shrinkWrap: true,
            children: List.generate(infoMap.length, (index) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(infoMap.keys.elementAt(index), style: subtitle2(color: tertiary)),
                  Text(infoMap[infoMap.keys.elementAt(index)], style: bodyText2(color: shadow)),
                ],
              );
            }),
          ),
        ],
      ),
    );
  }
}

class VehiclePanel extends StatelessWidget {
  const VehiclePanel({
    Key? key,
    required this.vehiculos,
  }) : super(key: key);
  final List<String> vehiculos;

  @override
  Widget build(BuildContext context) {
    if (vehiculos.isNotEmpty) {
      return Container(
        height: 130,
        padding: const EdgeInsets.symmetric(vertical: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("VEHÍCULOS:", style: bodyText1(color: tertiary)),
            Expanded(
              child: ListView.builder(
                itemCount: vehiculos.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    minLeadingWidth: 25,
                    leading: Text("${index + 1}", style: Theme.of(context).textTheme.headline3),
                    title: Text(vehiculos[index]),
                    subtitle: Text("Modelo: ${vehiculos[index]}"),
                  );
                },
              ),
            ),
          ],
        ),
      );
    } else {
      return Container();
    }
  }
}
