﻿import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

// COLORES
const Color primary = Color(0xFF1f2b2d);
const Color secondary = Color(0xFF0d398b);
const Color tertiary = Color(0xFFfefcfb);
const Color accent = Color(0xFF53ff8a);
const Color dark = Color(0xFF111b1e);
const Color backgroundColor = Color(0xFF121212);
const Color shadow = Color(0xFFadadad);
const Color light = Color(0xFF446b87);
const Color success = Color(0xFFb2ff59);
const Color warning = Color(0xFFcfc943);
const Color danger = Color(0xFFe61210);

/* BACKGROUND DECORATION IMAGE */
const DecorationImage backgroundDecorationImage = DecorationImage(
  image: AssetImage('assets/images/space.jpg'),
  fit: BoxFit.cover,
);

/* RADAR IMAGES */
const String radarImage = 'https://i.pinimg.com/564x/19/b6/f9/19b6f9f8b95dc87ebee2c7dc1e68b7ab.jpg';
// https://i.pinimg.com/originals/7f/96/24/7f9624334d322f6c0f2268ac2db7a0db.gif
// https://i.pinimg.com/originals/41/59/df/4159df984ab3f4ae2191c4bdd7670fc4.gif
// https://i.pinimg.com/564x/19/b6/f9/19b6f9f8b95dc87ebee2c7dc1e68b7ab.jpg
// https://i.pinimg.com/564x/09/f3/4c/09f34c4f92e78ac8e05f53afbe7a7223.jpg
// https://i.pinimg.com/564x/ab/e5/e2/abe5e278a9748bf1c40fa2eac610b834.jpg
// https://i.pinimg.com/originals/b5/d7/6e/b5d76e8dd3b691a765ae19e9bed9629d.gif
// https://i.pinimg.com/564x/37/d1/fa/37d1faae1a96311dca866bbb9ec996a6.jpg

// FUENTES
TextTheme swTextTheme = TextTheme(
  headline1: GoogleFonts.chakraPetch(fontSize: 64, fontWeight: FontWeight.bold, letterSpacing: -1.5, color: tertiary),
  headline2: GoogleFonts.chakraPetch(fontSize: 48, fontWeight: FontWeight.w600, letterSpacing: -0.5, color: tertiary),
  headline3: GoogleFonts.chakraPetch(fontSize: 34, fontWeight: FontWeight.w500, letterSpacing: 0.0, color: tertiary),
  headline4: GoogleFonts.chakraPetch(fontSize: 24, fontWeight: FontWeight.normal, letterSpacing: 0.25, color: tertiary),
  headline5: GoogleFonts.chakraPetch(fontSize: 20, fontWeight: FontWeight.normal, letterSpacing: 0.0, color: tertiary),
  headline6: GoogleFonts.chakraPetch(fontSize: 18, fontWeight: FontWeight.w500, letterSpacing: 0.15, color: tertiary),
  bodyText1: GoogleFonts.chakraPetch(fontSize: 16, fontWeight: FontWeight.w500, letterSpacing: 0.5, color: tertiary),
  bodyText2: GoogleFonts.chakraPetch(fontSize: 14, fontWeight: FontWeight.normal, letterSpacing: 0.25, color: tertiary),
  subtitle1: GoogleFonts.chakraPetch(fontSize: 16, fontWeight: FontWeight.normal, letterSpacing: 0.15, color: tertiary),
  subtitle2: GoogleFonts.chakraPetch(fontSize: 14, fontWeight: FontWeight.w500, letterSpacing: 0.1, color: tertiary),
);

// FUENTE CUSTOM COLOR
TextStyle headline1({required Color color}) => GoogleFonts.chakraPetch(fontSize: 64, fontWeight: FontWeight.bold, letterSpacing: -1.5, color: color);
TextStyle headline2({required Color color}) => GoogleFonts.chakraPetch(fontSize: 48, fontWeight: FontWeight.w600, letterSpacing: -0.5, color: color);
TextStyle headline3({required Color color}) => GoogleFonts.chakraPetch(fontSize: 34, fontWeight: FontWeight.w500, letterSpacing: 0.0, color: color);
TextStyle headline4({required Color color}) => GoogleFonts.chakraPetch(fontSize: 24, fontWeight: FontWeight.normal, letterSpacing: 0.25, color: color);
TextStyle headline5({required Color color}) => GoogleFonts.chakraPetch(fontSize: 20, fontWeight: FontWeight.normal, letterSpacing: 0.0, color: color);
TextStyle headline6({required Color color}) => GoogleFonts.chakraPetch(fontSize: 18, fontWeight: FontWeight.w500, letterSpacing: 0.15, color: color);
TextStyle bodyExtended({required Color color}) => GoogleFonts.chakraPetch(fontSize: 16, fontWeight: FontWeight.w500, letterSpacing: 1.5, color: color);
TextStyle bodyText1({required Color color}) => GoogleFonts.chakraPetch(fontSize: 16, fontWeight: FontWeight.w500, letterSpacing: 0.5, color: color);
TextStyle bodyText2({required Color color}) => GoogleFonts.chakraPetch(fontSize: 14, fontWeight: FontWeight.normal, letterSpacing: 0.25, color: color);
TextStyle subtitle1({required Color color}) => GoogleFonts.chakraPetch(fontSize: 16, fontWeight: FontWeight.normal, letterSpacing: 0.15, color: color);
TextStyle subtitle2({required Color color}) => GoogleFonts.chakraPetch(fontSize: 14, fontWeight: FontWeight.w500, letterSpacing: 0.1, color: color);
