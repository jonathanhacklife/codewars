# Desafío Flutter - Swapi

Desafío hecho en Flutter con comunicación a la api de Star Wars (Swapi) para Urbetrack.

[Jonathan Wildemer / Desafios Flutter · GitLab](https://gitlab.com/jonathanhacklife/desafios-flutter)

## **Comenzando 🚀**

Para clonar el repositorio localmente:

```
git clone https://gitlab.com/jonathanhacklife/desafios-flutter
```

💡 Para ejecutar la aplicación en modo debug es opcional ejecutar el emulador o conectar un dispositivo para visualizarlo en tiempo real.

## **Instalación 🔧**

### **Pre-requisitos 📋**

Para instalar el archivo APK en el dispositivo Android debe habilitar las opciones de desarrollo.

```
https://www.xatakandroid.com/tutoriales/como-instalar-aplicaciones-en-apk-en-un-movil-android
```

## **Construido con 🛠️**

- [Flutter](https://flutter.dev/) - El framework mobile utilizado
- BLoC - Manejador de estados
- [Excalidraw](https://excalidraw.com/) - Usado para armar el boceto de la aplicación
- [Figma](https://www.figma.com/) - Prototipado de interfaz en media

## **Versionado 📌**

Se usó GIT para el versionado. Para todas las versiones disponibles, mira los [commits en este repositorio](https://gitlab.com/jonathanhacklife/codewars/-/commits/master).

## **Expresiones de Gratitud 🎁**

- Gracias a Urbetrack por la posibilidad de presentar el desafío a pesar de las dificultades en los tiempos empleados para el desarrollo del mismo 🤓.